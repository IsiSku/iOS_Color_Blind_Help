Project initialization.

# iOS Color Blind Help

The purpose of this project is to create an iPhone app which helps people with
colour blindness or colour impaired vision to differentiate between surfaces
which they see as the same colour.

Target device is an iPhone 6s due to availability constraints.
Other options include 6 and 5 models later in development.

The project is divided into several stages:
* 20th of January
    - Recognizing surfaces of certain characteristics in a video
* 25th of February
    - Inserting virtual objects into real video
    - Programming abstractions in machine vision
* 1st of March
    - Analysis of compression techniques in mobile device video systems
* 1st of June
    - Training game for new users

### Tools

* Xcode
* Open CV

### Literature and useful links

* Tools
    - [Open CV official docs] (http://docs.opencv.org/2.4/doc/tutorials/ios/table_of_content_ios/table_of_content_ios.html)
    - [Open CV Moments] (https://www.youtube.com/channel/UCuKtEaX1LN3rxHGgmUOqn8g)
* Colour correction models
    - [Neda Milic] (http://www.grid.uns.ac.rs/data/biblioteka/disertacije/neda_milic_disertacije_final.pdf)

