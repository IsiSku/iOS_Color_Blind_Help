//
//  OpenCVUtil.mm
//  SurfaceColourDetection
//
//  Created by Isidora Skulec on 1/11/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import "OpenCVUtil.h"

@implementation OpenCVUtil

+(NSString *) getOpenCVVersion
{
    return [NSString stringWithFormat:@"Open CV Ver. %s", CV_VERSION];
}

@end
