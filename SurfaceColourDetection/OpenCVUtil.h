//
//  OpenCVUtil.h
//  SurfaceColourDetection
//
//  Created by Isidora Skulec on 1/11/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenCVUtil : NSObject

+(NSString *) getOpenCVVersion;

@end
