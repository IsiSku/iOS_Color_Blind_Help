//
//  AppDelegate.swift
//  SurfaceColourDetection
//
//  Created by Isidora Skulec on 1/11/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

