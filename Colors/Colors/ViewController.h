//
//  ViewController.h
//  Colors
//
//  Created by Isidora Skulec on 1/20/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

#import <opencv2/highgui/cap_ios.h>
using namespace cv;
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<CvVideoCameraDelegate>


@end

