//
//  ViewController.m
//  Colors
//
//  Created by Isidora Skulec on 1/20/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

#import "ViewController.h"
#import <opencv2/imgproc.hpp>

@interface ViewController ()

@property (nonatomic, retain) CvVideoCamera* camBack;
@property (nonatomic, retain) CvVideoCamera* camFront;
@property bool isBack;
@property bool isTorchOn;
@property bool isMorphOn;

@property (weak, nonatomic) IBOutlet UILabel *lblCameraUnavailable;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIImageView *vwPreview;

@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnHelp;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnResume;
@property (weak, nonatomic) IBOutlet UIButton *btnMorph;

@property (weak, nonatomic) IBOutlet UISwitch *swFilterOn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sgFilterMode;

@property (weak, nonatomic) IBOutlet UISlider *sldYLo;
@property (weak, nonatomic) IBOutlet UISlider *sldYHi;
@property (weak, nonatomic) IBOutlet UILabel *lblYLo;
@property (weak, nonatomic) IBOutlet UILabel *lblYHi;

@property (weak, nonatomic) IBOutlet UISlider *sldCbLo;
@property (weak, nonatomic) IBOutlet UISlider *sldCbHi;
@property (weak, nonatomic) IBOutlet UILabel *lblCbLo;
@property (weak, nonatomic) IBOutlet UILabel *lblCbHi;

@property (weak, nonatomic) IBOutlet UISlider *sldCrLo;
@property (weak, nonatomic) IBOutlet UISlider *sldCrHi;
@property (weak, nonatomic) IBOutlet UILabel *lblCrLo;
@property (weak, nonatomic) IBOutlet UILabel *lblCrHi;

@property int selectedFilter;

@end

@implementation ViewController

/*
 Orange  0-22
 Yellow 22- 38
 Green 38-75
 Blue 75-130
 Violet 130-160
 Red 160-179
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.camBack = [[CvVideoCamera alloc] initWithParentView:self.vwPreview];
    self.camBack.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.camBack.defaultAVCaptureSessionPreset = AVCaptureSessionPresetPhoto;
    self.camBack.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.camBack.defaultFPS = 60;
    self.camBack.grayscaleMode = NO;
    self.camBack.delegate = self;
    self.camBack.rotateVideo = YES;
    [self.camBack unlockBalance];
    [self.camBack unlockFocus];
    [self.camBack unlockExposure];
    
    
    self.camFront = [[CvVideoCamera alloc] initWithParentView:self.vwPreview];
    self.camFront.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.camFront.defaultAVCaptureSessionPreset = AVCaptureSessionPresetPhoto;
    self.camFront.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.camFront.defaultFPS = 60;
    self.camFront.grayscaleMode = NO;
    self.camFront.delegate = self;
    self.camFront.rotateVideo = YES;
    
    self.isBack = true;
    self.isTorchOn = false;
    self.isMorphOn = false;
    self.selectedFilter = 0;
    
    [self.camBack start];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchCamera:(id)sender {
    [self.lblStatus setText:(@"Status: camera")];
    
    if (self.isBack) {
        self.isBack = false;
        [self.camBack stop];
        [self.camFront start];
    }
    else {
        self.isBack = true;
        [self.camFront stop];
        [self.camBack start];
    }
}

- (IBAction)switchMorph:(id)sender {
    self.isMorphOn = !self.isMorphOn;
}


- (IBAction)showHelp:(id)sender {
    [self.lblStatus setText:(@"Status: help")];
}

- (IBAction)turnFilter:(id)sender {
    if (self.swFilterOn.isOn) {
        [self.lblStatus setText:(@"Status: filter on")];
        self.sgFilterMode.hidden = false;
    }
    else {
        [self.lblStatus setText:(@"Status: filter off")];
        self.sgFilterMode.hidden = true;
    }
}

- (IBAction)showSettings:(id)sender {
    [self.lblStatus setText:(@"Status: settings")];
    
    if (self.isBack && !self.isTorchOn) {
        printf("[TORCH] Camera is back: ");
        NSArray *devices = self.camBack.captureSession.inputs;
        for (AVCaptureDeviceInput *device in devices) {
            printf("Found a device... ");
            if ([device.device hasTorch]) {
                printf("+ ...it has a torch");
                [device.device lockForConfiguration:nil];
                [device.device setTorchMode:AVCaptureTorchModeOn];
                [device.device unlockForConfiguration];
                self.isTorchOn = true;
                break;
            }
        }
    }
    else if (self.isBack && self.isTorchOn) {
        printf("[TORCH] Camera is back: ");
        NSArray *devices = self.camBack.captureSession.inputs;
        for (AVCaptureDeviceInput *device in devices) {
            printf("Found a device... ");
            if ([device.device hasTorch]) {
                printf("+ ...it has a torch");
                [device.device lockForConfiguration:nil];
                [device.device setTorchMode:AVCaptureTorchModeOff];
                [device.device unlockForConfiguration];
                self.isTorchOn = false;
                break;
            }
        }
    }
}

- (IBAction)switchFilter:(id)sender {
    switch (self.sgFilterMode.selectedSegmentIndex) {
        case 0:
            [self.lblStatus setText:(@"Status: protan")];
            self.selectedFilter = 0;
            break;
        case 1:
            [self.lblStatus setText:(@"Status: deutan")];
            self.selectedFilter = 1;
            break;
        case 2:
            [self.lblStatus setText:(@"Status: tritan")];
            self.selectedFilter = 2;
            break;
        default:
            [self.lblStatus setText:(@"Status: no filter")];
            break;
    }
}

- (IBAction)resume:(id)sender {
    [self.lblStatus setText:(@"Status: resume")];
}

#pragma mark - Protocol CvVideoCameraDelegate

- (IBAction)YLoChanged:(id)sender {
    self.lblYLo.text = [NSString stringWithFormat:@"%.f", self.sldYLo.value];
    ylo = self.sldYLo.value;
}
- (IBAction)YHiChanged:(id)sender {
    self.lblYHi.text = [NSString stringWithFormat:@"%.f", self.sldYHi.value];
    yhi = self.sldYHi.value;
}
- (IBAction)RloChanged:(id)sender {
    self.lblCrLo.text = [NSString stringWithFormat:@"%.f", self.sldCrLo.value];
    rlo = self.sldCrLo.value;
}
- (IBAction)RhiChanged:(id)sender {
    self.lblCrHi.text = [NSString stringWithFormat:@"%.f", self.sldCrHi.value];
    rhi = self.sldCrHi.value;
}
- (IBAction)BLoChanged:(id)sender {
    self.lblCbLo.text = [NSString stringWithFormat:@"%.f", self.sldCbLo.value];
    blo = self.sldCbLo.value;
}
- (IBAction)BHiChanged:(id)sender {
    self.lblCbHi.text = [NSString stringWithFormat:@"%.f", self.sldCbHi.value];
    bhi = self.sldCbHi.value;
}


int ylo = 0;
int yhi = 255;
int rlo = 0;
int rhi = 255;
int blo = 0;
int bhi = 255;

static int avgval = 0;

#ifdef __cplusplus
-(void)processImage:(cv::Mat &)image {
    
    if (self.swFilterOn.isOn) {
        
        Mat image_copy;
        cv::cvtColor(image, image_copy, CV_RGB2YCrCb);
        
        Mat imgThresholded;
        Mat imgThresholded2;
        Mat imgThresholded3;
        Mat chan[3];
        split(image_copy, chan);
        
        Scalar luma = mean(chan[0]);
        avgval = floor(luma[0]);
        printf("Avg luma %d", avgval);
        
        switch (self.selectedFilter) {
            case 0: // red
                
                // Y000 - Y050 range
                inRange(image_copy,
                        Scalar(100, 0, 182),
                        Scalar(255, 138, 255),
                        imgThresholded);
                
                // Y050 - Y100 range
                inRange(image_copy,
                        Scalar(0, 70, 170),
                        Scalar(110, 130, 255),
                        imgThresholded2);
                
                addWeighted(imgThresholded, 1.0, imgThresholded2, 1.0, 1.0, imgThresholded);
                
                break;
            case 1: // green
                // Y000 - Y050 range
                inRange(image_copy,
                        Scalar(0, 0, 0),
                        Scalar(174, 116, 130),
                        imgThresholded);
                
                // Y050 - Y100 range
                inRange(image_copy,
                        Scalar(108, 0, 0),
                        Scalar(255, 99, 136),
                        imgThresholded2);
                
                addWeighted(imgThresholded, 1.0, imgThresholded2, 1.0, 1.0, imgThresholded);
                
                break;
            case 2: // blue
                // Y000 - Y050 range
                inRange(image_copy,
                        Scalar(0, 148, 0),
                        Scalar(124, 255, 117),
                        imgThresholded);
                
                // Y050 - Y100 range
                inRange(image_copy,
                        Scalar(99, 131, 0),
                        Scalar(255, 210, 107),
                        imgThresholded2);
                
                addWeighted(imgThresholded, 1.0, imgThresholded2, 1.0, 1.0, imgThresholded);
                
                break;
            default:
                break;
        }
        
        
        // Obtain the pattern to be combined with the mask
        
        int cols = imgThresholded.cols;
        int rows = imgThresholded.rows;
        
        UIImage *imgPattern = [UIImage imageNamed:@"stripes"];
        Mat dotts = [self cvMatFromUIImage:imgPattern];
        Mat pat;
        cvtColor(dotts, pat, CV_BGR2GRAY);
        
        int pCols = pat.cols;
        int pRows = pat.rows;
        
        int nx = (cols / pCols) + 1;
        int ny = (rows / pRows) + 1;
        
        Mat repeated;
        repeat(pat, ny, nx, repeated);
        Mat crop = repeated(cv::Rect(0, 0, cols, rows));
        
        // Get mask which is threshold and stripes
        bitwise_and(crop, imgThresholded, imgThresholded);
        
        bitwise_not(imgThresholded, imgThresholded);
        cvtColor(imgThresholded, imgThresholded, CV_GRAY2RGBA);
        bitwise_and(image, imgThresholded, image);
    }
}

- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

- (cv::Mat)cvMatGrayFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    return cvMat;
}

void UIImageToMat(const UIImage* image,
                  cv::Mat& m, bool alphaExist) {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = CGImageGetWidth(image.CGImage), rows = CGImageGetHeight(image.CGImage);
    CGContextRef contextRef;
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast;
    if (CGColorSpaceGetModel(colorSpace) == kCGColorSpaceModelMonochrome)
    {
        m.create(rows, cols, CV_8UC1); // 8 bits per component, 1 channel
        bitmapInfo = kCGImageAlphaNone;
        if (!alphaExist)
            bitmapInfo = kCGImageAlphaNone;
        else
            m = cv::Scalar(0);
        contextRef = CGBitmapContextCreate(m.data, m.cols, m.rows, 8,
                                           m.step[0], colorSpace,
                                           bitmapInfo);
    }
    else
    {
        m.create(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
        if (!alphaExist)
            bitmapInfo = kCGImageAlphaNoneSkipLast |
            kCGBitmapByteOrderDefault;
        else
            m = cv::Scalar(0);
        contextRef = CGBitmapContextCreate(m.data, m.cols, m.rows, 8,
                                           m.step[0], colorSpace,
                                           bitmapInfo);
    }
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows),
                       image.CGImage);
    CGContextRelease(contextRef);
}

#endif

@end
