//
//  AppDelegate.h
//  Colors
//
//  Created by Isidora Skulec on 1/20/17.
//  Copyright © 2017 Isidora Skulec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

